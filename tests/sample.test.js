function add (n1, n2) {
    return n1 + n2 + 2;
}

describe('Should run the sample tests', () => {
    it('Should add 2 numbers', () => {
        expect(add (2,3)).toEqual(5);
    });
});